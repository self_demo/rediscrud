package com.api.redis.controller;

import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.service.annotation.GetExchange;

import com.api.redis.dao.UserDao;
import com.api.redis.model.User;

@RestController
@RequestMapping("/users")
public class Usercontroller {

	@Autowired
	private UserDao userDao;

	@PostMapping
	public User saveUser(@RequestBody User user) {
		System.err.println("In save");
		user.setUserId(UUID.randomUUID().toString());
		return userDao.save(user);

	}

	@GetMapping("/{userId}")
	public User getUser(@PathVariable String userId) {
		return userDao.get(userId);
	}

	@GetMapping
	public Map<Object, Object> getAll() {
		return userDao.findAll();
	}

	@DeleteMapping("/{userId}")
	public void deleteUser(@PathVariable  String userId) {
		userDao.delete(userId);
	}
}
